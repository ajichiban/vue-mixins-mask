## Instalacion ##
`npm install vue-mixins-masK`

## Implementacion ##
importa el plugin y agregalo en seccion de 'mixins' del componente.

~~~
    import maskMix from 'vue-mixins-masK'
    export default {
        ...

        mixins: [maskMix],

        ...
~~~

Luego en el template del componente agrega la directiva v-ichi-mask; en esta directiva agrega el patron que quieres que tenga tu input ejemplo: '##-##-##', donde el simbolo '#' corresponde a cualquier caracter alfanumerico. Y añade ell v-model haciendo binding al modelo maskValue.

~~~
<template>
    ...
    <input 
        v-ichi-mask="'##-##-###'"
        v-model="maskValue" 
        class="form-control" 
        placeholder="rut"
    >
    ...
~~~









