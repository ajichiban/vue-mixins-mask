export default {

	created(){
		console.log('maskMix active !');
	},

	data(){
		return {
			maskPather: '',
			maskValue: '',
			maskCount:0
		}
	},
	directives: {

		ichiMask:{
			bind(el, binding, vnode){
				console.log('vnode: ', vnode.context.$datamaskPather);

				vnode.context.maskPather = binding.value

			}
		}
	},
	watch:{
		maskValue(val){
			this.maskValue = this.$pluginsFormatter(val)

		}
	},

	methods: {

		$pluginsFormatter(val){

			this.maskCount = 0

			let exReg = /[\.\,\-\+\*\)\(\_\s\"\'\¡\!\$\%\&\/\=\?\¿\}\{\]\[]/g

			let valClean = val.replace(exReg, '')

			let patherClenned = this.maskPather.replace(exReg, '').length

			if (valClean.length > patherClenned ) {

				val = val.split("")
				val.pop()
				val = val.join("")

				return val
			}
			
			let valCleanned =  val.replace(exReg, '').split('')


			let valFormated = []

			let arrayPather = this.maskPather.split("")

			//Segun el pather.
			valCleanned.forEach((char, index)=>{
				this.recursive(this.maskPather, valFormated)
				this.maskCount++
				valFormated.push(char)

			

				if (index+1 === patherClenned) {
					console.log('maskCount', this.maskCount);
					this.recursive(this.maskPather, valFormated)
				}
			})


			valFormated = valFormated.join("")

			return valFormated
		},

		recursive(pather,array){
			if(pather[this.maskCount] && pather[this.maskCount] !== '#'){
				console.log('pather-test: ', pather[this.maskCount]);
				array.push(pather[this.maskCount])
				this.maskCount++
				this.recursive(pather, array)
			}
		},
	}

} 